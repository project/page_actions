<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Define styles for page actions.
 *
 * This hook allows modules to specify new styles for page actions.
 *
 * @return
 *   An array of information for styles.
 *   The array contains a sub-array for each style, with the style name as the
 *   key. Style names may only contain lowercase alhpa-numeric characters and
 *   underscores. Possible attributes for each sub-array are:
 *   - "title": Required. The human readable name of the style.
 *   - "css": Optional. An associative array containing one or both of
 *     following:
 *     - "code": A string of CSS code that will be loaded via CTools CSS tool.
 *       If your style requires this feature, be sure to ensure CTools exists
 *       before exposing the style.
 *     - "file": The file name of a CSS file to load.
 *     - "file path": The path to the CSS file specified above. When omitted
 *       and a "file" is specified, the path to the module directory will be
 *       used.
 *   - "js": Optional. An associative array containing the following:
 *     - "file": The file name of a JavaScript file to load.
 *     - "file path": The path to the JavaScript file specified above. When
 *       omitted, the path to the module directory will be used.
 *   - "settings": Optional. An associative array of settings to be passed to
 *     JavaScript. From JavaScript, settings can be accessed from
 *     Drupal.settings.pageActions. The following are default settings which
 *     can be overridden:
 *     - "adjustMarginBottom": Adjust the body margin-bottom to the height of
 *       the #page-actions div. Defaults to TRUE.
 */
function hook_page_actions_styles() {
  $styles['clean'] = array(
    'title' => t('Clean'),
    'css' => array(
      'code' => '#page-actions {width: 100%;}',
      'file' => 'style.css',
    ),
    'js' => array(
      'file' => 'script.js',
    ),
    'settings' => array(
      'adjustMarginBottom' => FALSE,
    ),
  );
  return $styles;
}
